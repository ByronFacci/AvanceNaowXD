package com.firebase.proyecto_movil_naow.Actividades;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.preference.EditTextPreference;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.firebase.proyecto_movil_naow.Modelo.Usuario;
import com.firebase.proyecto_movil_naow.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsuarioNuevo extends AppCompatActivity implements View.OnClickListener{

    private EditText nombre, apellido, cedula, celular, correo, clave, vclave;
    private Button registrar, IniciarSesion, OlvidoClave;
    private ViewFlipper viewFlipper;
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_nuevo);

        nombre =(EditText)findViewById(R.id.TXTNombres);
        apellido = (EditText)findViewById(R.id.TXTApellidos);
        cedula = (EditText)findViewById(R.id.TXTCedula);
        celular = (EditText)findViewById(R.id.TXTCelular);
        correo = (EditText)findViewById(R.id.TXTCorreo);
        clave = (EditText)findViewById(R.id.TXTClave);
        vclave = (EditText)findViewById(R.id.TXTVClave);
        viewFlipper = (ViewFlipper)findViewById(R.id.ViewFliper2);
        registrar = (Button)findViewById(R.id.BTNRegistrar);
        IniciarSesion = (Button)findViewById(R.id.BTNIniciarS);
        OlvidoClave = (Button)findViewById(R.id.BTNRestablecer);
        progressDialog = new ProgressDialog(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        users = firebaseDatabase.getReference("Users");
        registrar.setOnClickListener(this);
        IniciarSesion.setOnClickListener(this);
        OlvidoClave.setOnClickListener(this);
               int imagenes[] = {R.drawable.energy, R.drawable.alcalina, R.drawable.logo, R.drawable.gas, R.drawable.smart};
        for (int i = 0; i< imagenes.length; i++){
            fliper(imagenes[i]);
        }
    }

    public void fliper(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);

        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
           case R.id.BTNRegistrar:
               if (nombre.getText().toString().isEmpty()){
                   nombre.setError(getString(R.string.error));
                   nombre.requestFocus();
               }else if (apellido.getText().toString().isEmpty()){
                   apellido.setError(getString(R.string.error));
                   apellido.requestFocus();
               }else if (cedula.getText().toString().isEmpty()){
                   cedula.setError(getString(R.string.error));
                   cedula.requestFocus();
               }else if (celular.getText().toString().isEmpty()){
                   celular.setError(getString(R.string.error));
                   celular.requestFocus();
               }else if (correo.getText().toString().isEmpty()){
                   correo.setError(getString(R.string.error));
                   correo.requestFocus();
               }else if (clave.getText().toString().isEmpty()){
                   clave.setError(getString(R.string.error));
                   clave.requestFocus();
               }else if (vclave.getText().toString().isEmpty()) {
                   vclave.setError(getString(R.string.error));
                   vclave.requestFocus();
               }else if (CedulaCorrecta(cedula.getText().toString())==false) {
                   cedula.setError(getString(R.string.cedulacorrecta));
                   cedula.setText("");
                   cedula.requestFocus();
               }else if (validar_clave(clave.getText().toString())==true){
                   clave.setError(getString(R.string.clavevalidar));
                   clave.requestFocus();
               }else if(vclave.getText().toString().equals(clave.getText().toString())==false) {
                   vclave.setError(getString(R.string.comprobarclave) + " " + getString(R.string.password) + " " + getString(R.string.xxx));
                   vclave.requestFocus();
               }else if (validate_correo(correo.getText().toString())==false){
                   correo.setError(getString(R.string.correovalido));
                   correo.requestFocus();
               }else {
                    //Va Codigo De Registrar dx..!
                    progressDialog.setTitle("REGISTRO");
                    progressDialog.setMessage("REGISTRANDO USUARIO..!");
                    progressDialog.show();
                    firebaseAuth.createUserWithEmailAndPassword(correo.getText().toString(), clave.getText().toString())
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult){
                                    Usuario usuario = new Usuario();
                                    usuario.setNombres(nombre.getText().toString());
                                    usuario.setApellidos(apellido.getText().toString());
                                    usuario.setCedula(cedula.getText().toString());
                                    usuario.setCelular(celular.getText().toString());
                                    usuario.setCorreo(correo.getText().toString());
                                    usuario.setClave(clave.getText().toString());
                                    usuario.setVclave(vclave.getText().toString());

                                    users.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(UsuarioNuevo.this, "USUARIO REGISTRADO..!", Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(UsuarioNuevo.this, Login.class);
                                            startActivity(intent);
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(UsuarioNuevo.this, "ERROR AL REGISTRAR USUARIO..!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(UsuarioNuevo.this, "ERROR..!", Toast.LENGTH_LONG).show();
                        }
                    });
                    Toast.makeText(UsuarioNuevo.this, "CORRECTO", Toast.LENGTH_LONG).show();

               }
               break;

           case R.id.BTNIniciarS:
                Intent intent = new Intent(UsuarioNuevo.this, Login.class);
                startActivity(intent);
                break;

            case R.id.BTNRecuperar:
                Toast.makeText(UsuarioNuevo.this, "AUN NO SE ENCUENTRA LISTO..!", Toast.LENGTH_LONG).show();
                break;
        }


    }


    public boolean CedulaCorrecta(String cedula) {
        boolean cedulaCorrecta = false;
        try {

            if (cedula.length() == 10)
            {
                int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
                if (tercerDigito < 6) {
                    int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
                    int verificador = Integer.parseInt(cedula.substring(9,10));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < (cedula.length() - 1); i++) {
                        digito = Integer.parseInt(cedula.substring(i, i + 1))* coefValCedula[i];
                        suma += ((digito % 10) + (digito / 10));
                    }

                    if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                        cedulaCorrecta = true;
                    }
                    else if ((10 - (suma % 10)) == verificador) {
                        cedulaCorrecta = true;
                    } else {
                        cedulaCorrecta = false;
                    }
                } else {
                    cedulaCorrecta = false;
                }
            } else {
                cedulaCorrecta = false;
            }
        } catch (NumberFormatException nfe) {
            cedulaCorrecta = false;
        } catch (Exception err) {
            cedulaCorrecta = false;
        }

        if (!cedulaCorrecta) {
        }
        return cedulaCorrecta;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }

    public boolean validar_clave(String password){
        String PASSWORD_PATTERN ="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean validate_correo(String email){
        String EMAIL_ADDRESS ="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern1 = Pattern.compile(EMAIL_ADDRESS);
        Matcher matcher1 = pattern1.matcher(email);
        return matcher1.matches();
    }

}
