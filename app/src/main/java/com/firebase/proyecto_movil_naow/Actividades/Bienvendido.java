package com.firebase.proyecto_movil_naow.Actividades;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.proyecto_movil_naow.Modelo.Usuario;
import com.firebase.proyecto_movil_naow.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.KeyStore;
import java.util.ArrayList;

public class Bienvendido extends AppCompatActivity {

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener Auth;
    private DatabaseReference users;
    private String userId;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvendido);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        users = firebaseDatabase.getReference();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        mListView = (ListView) findViewById(R.id.listview);
        userId = user.getUid();


        Auth = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user!=null){

                }else{
                    //error
                }

            }
        };

        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    private void showData(DataSnapshot dataSnapshot) {
        for(DataSnapshot ds : dataSnapshot.getChildren()){
            Usuario usuario = new Usuario();
            usuario.setNombres("Nombres: "+ds.child(userId).getValue(Usuario.class).getNombres());
            usuario.setApellidos("Apellidos: "+ds.child(userId).getValue(Usuario.class).getApellidos());
            usuario.setCedula("Cédula: "+ds.child(userId).getValue(Usuario.class).getCedula());
            usuario.setCelular("Celular: "+ds.child(userId).getValue(Usuario.class).getCelular());
            usuario.setCorreo("Correo: "+ds.child(userId).getValue(Usuario.class).getCorreo());

            ArrayList<String> array  = new ArrayList<>();
            array.add(usuario.getNombres());
            array.add(usuario.getApellidos());
            array.add(usuario.getCedula());
            array.add(usuario.getCelular());
            array.add(usuario.getCorreo());
            ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,array);
            
            mListView.setAdapter(adapter);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(Auth);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Auth != null) {
            firebaseAuth.removeAuthStateListener(Auth);
        }
    }
}
