package com.firebase.proyecto_movil_naow.Modelo;

public class Usuario {

    private String nombres, apellidos, cedula, celular, correo, clave, vclave;

    public Usuario(){
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.celular = celular;
        this.correo = correo;
        this.clave = clave;
        this.vclave = vclave;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getVclave() {
        return vclave;
    }

    public void setVclave(String vclave) {
        this.vclave = vclave;
    }
}


